module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs61160155/learn_bootstrap/'
      : '/'
}
